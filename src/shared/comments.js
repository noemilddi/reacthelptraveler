export const COMMENTS = 
[
    {
        id: 0,
        userId: 0,
        rating: 5,
        comment: "Great experience, I have visited many sites!",
        author: "Martin Aurell",
        date: "2012-10-16T17:57:28.556094Z"
    },
    {
        id: 1,
        userId: 0,
        rating: 4,
        comment: "I will repeat without hesitation this experience!",
        author: "Garde Saiz",
        date: "2014-09-05T17:57:28.556094Z"
    },
    {
        id: 2,
        userId: 0,
        rating: 3,
        comment: "Totally recommended, I have learned a lot!",
        author: "Reyes Pausa",
        date: "2015-02-13T17:57:28.556094Z"
    },
    {
        id: 3,
        userId: 0,
        rating: 4,
        comment: "I have met many fantastic and fun people!",
        author: "Sotomayor Vergara",
        date: "2013-12-02T17:57:28.556094Z"
    },
    {
        id: 4,
        userId: 0,
        rating: 2,
        comment: "I have learned a new language very fast!",
        author: "Garcia Davila",
        date: "2011-12-02T17:57:28.556094Z"
    },
    {
        id: 5,
        userId: 1,
        rating: 5,
        comment: "Great experience, I have visited many sites!",
        author: "Martin Aurell",
        date: "2012-10-16T17:57:28.556094Z"
    },
    {
        id: 6,
        userId: 1,
        rating: 4,
        comment: "I will repeat without hesitation this experience!",
        author: "Garde Saiz",
        date: "2014-09-05T17:57:28.556094Z"
    },
    {
        id: 7,
        userId: 1,
        rating: 3,
        comment: "Totally recommended, I have learned a lot!",
        author: "Reyes Pausa",
        date: "2015-02-13T17:57:28.556094Z"
    },
    {
        id: 8,
        userId: 1,
        rating: 4,
        comment: "I have met many fantastic and fun people!",
        author: "Sotomayor Vergara",
        date: "2013-12-02T17:57:28.556094Z"
    },
    {
        id: 9,
        userId: 1,
        rating: 2,
        comment: "I have learned a new language very fast!",
        author: "Garcia Davila",
        date: "2011-12-02T17:57:28.556094Z"
    },
    {
        id: 10,
        userId: 2,
        rating: 5,
        comment: "Great experience, I have visited many sites!",
        author: "Martin Aurell",
        date: "2012-10-16T17:57:28.556094Z"
    },
    {
        id: 11,
        userId: 2,
        rating: 4,
        comment: "I will repeat without hesitation this experience!",
        author: "Garde Saiz",
        date: "2014-09-05T17:57:28.556094Z"
    },
    {
        id: 12,
        userId: 2,
        rating: 3,
        comment: "Totally recommended, I have learned a lot!",
        author: "Reyes Pausa",
        date: "2015-02-13T17:57:28.556094Z"
    },
    {
        id: 13,
        userId: 2,
        rating: 4,
        comment: "I have met many fantastic and fun people!",
        author: "Sotomayor Vergara",
        date: "2013-12-02T17:57:28.556094Z"
    },
    {
        id: 14,
        userId: 2,
        rating: 2,
        comment: "I have learned a new language very fast!",
        author: "Garcia Davila",
        date: "2011-12-02T17:57:28.556094Z"
    },
    {
        id: 15,
        userId: 3,
        rating: 5,
        comment: "Great experience, I have visited many sites!",
        author: "Martin Aurell",
        date: "2012-10-16T17:57:28.556094Z"
    },
    {
        id: 16,
        userId: 3,
        rating: 4,
        comment: "I will repeat without hesitation this experience!",
        author: "Garde Saiz",
        date: "2014-09-05T17:57:28.556094Z"
    },
    {
        id: 17,
        userId: 3,
        rating: 3,
        comment: "Totally recommended, I have learned a lot!",
        author: "Reyes Pausa",
        date: "2015-02-13T17:57:28.556094Z"
    },
    {
        id: 18,
        userId: 3,
        rating: 4,
        comment: "I have met many fantastic and fun people!",
        author: "Sotomayor Vergara",
        date: "2013-12-02T17:57:28.556094Z"
    },
    {
        id: 19,
        userId: 3,
        rating: 2,
        comment: "I have learned a new language very fast!",
        author: "Garcia Davila",
        date: "2011-12-02T17:57:28.556094Z"
    },{
        id: 20,
        userId: 4,
        rating: 5,
        comment: "Great experience, I have visited many sites!",
        author: "Martin Aurell",
        date: "2012-10-16T17:57:28.556094Z"
    },
    {
        id: 21,
        userId: 4,
        rating: 4,
        comment: "I will repeat without hesitation this experience!",
        author: "Garde Saiz",
        date: "2014-09-05T17:57:28.556094Z"
    },
    {
        id: 22,
        userId: 4,
        rating: 3,
        comment: "Totally recommended, I have learned a lot!",
        author: "Reyes Pausa",
        date: "2015-02-13T17:57:28.556094Z"
    },
    {
        id: 23,
        userId: 4,
        rating: 4,
        comment: "I have met many fantastic and fun people!",
        author: "Sotomayor Vergara",
        date: "2013-12-02T17:57:28.556094Z"
    },
    {
        id: 24,
        userId: 4,
        rating: 2,
        comment: "I have learned a new language very fast!",
        author: "Garcia Davila",
        date: "2011-12-02T17:57:28.556094Z"
    },
    {
        id: 25,
        userId: 5,
        rating: 5,
        comment: "Great experience, I have visited many sites!",
        author: "Martin Aurell",
        date: "2012-10-16T17:57:28.556094Z"
    },
    {
        id: 26,
        userId: 5,
        rating: 4,
        comment: "I will repeat without hesitation this experience!",
        author: "Garde Saiz",
        date: "2014-09-05T17:57:28.556094Z"
    },
    {
        id: 27,
        userId: 5,
        rating: 3,
        comment: "Totally recommended, I have learned a lot!",
        author: "Reyes Pausa",
        date: "2015-02-13T17:57:28.556094Z"
    },
    {
        id: 28,
        userId: 5,
        rating: 4,
        comment: "I have met many fantastic and fun people!",
        author: "Sotomayor Vergara",
        date: "2013-12-02T17:57:28.556094Z"
    },
    {
        id: 29,
        userId: 5,
        rating: 2,
        comment: "I have learned a new language very fast!",
        author: "Garcia Davila",
        date: "2011-12-02T17:57:28.556094Z"
    }
];