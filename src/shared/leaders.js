export const LEADERS = [
    {
      id: 0,
      name: 'Paul',
      image: '/assets/images/paul.png',
      designation: 'Chief Executive Officer',
      abbr: 'CEO',
      featured: false,
      description: "Paul is the CEO of Help Traveler, of Spanish origin, from the province of Valencia, a technology lover and computer engineering student, he thought that if he liked to travel and know the world, other people could also like it and decided thanks To his knowledge of computer science put into motion this idea that contemplates the traditional trips of flight and hotel with the exchanges with other people. Paul is dedicated to hiring the different travel agencies so that users can choose between various flight and hotel options."
    },
    {
      id: 1,
      name: 'Noelia',
      image: '/assets/images/noelia.png',
      designation: 'Chief Technology Officer',
      abbr: 'CTO',
      featured: false,
      description: 'Noelia es CTO of Help Traveler, of Spain origin, Cuenca parents, but born in Valencia, a lover of sports, travel and know the world and web development, Noelia is in charge of leading the web development team, in her team the whole design is developed and then the functionality of the application. Noelia is one of the founders of this idea with Paul and Charles, and is also responsible for selecting the best developers so that the application has the best care and maintenance possible.'
    },
    {
      id: 2,
      name: 'Charles',
      image: '/assets/images/charles.png',
      designation: 'Chief Communications Officer',
      abbr: 'CCO',
      featured: false,
      description: 'Charles is CCO of Help Traveler, of Spanish origin, born in Valencia, who loves sports, travel and getting to know the world and international trade. Charles is in charge of leading the trade team, in his team they contact the clients of the website to coordinate the exchanges and also establish agreements of interest. Charles is one of the founders of this idea with Paul and Noelia, and is also responsible for selecting the best negotiators so that the application has the best possible care and maintenance.'
    }
  ];