export const USERS =
    [
        {
        id: 0,
        name:'Maria (Spain)',
        image: '/assets/images/maria.png',
        featured: true,
        description:'I am fun, I like meeting people and having fun with the people I love. In love with Spanish cuisine.'                    
        },
        {
        id: 1,
        name:'Ryu (Japan)',
        image: '/assets/images/ryu.png',
        featured: false,
        description:'I am a nature lover, environmentalist, and I like to share my hobbies with other people.'
        },
        {
        id: 2,
        name:'Frank (New York)',
        image: '/assets/images/frank.png',
        featured: false,
        description:'Lover of technology and motorcycles, I like to go out with my biker friends.'
        },
        {
        id: 3,
        name:'Chontel (Australia)',
        image: '/assets/images/chontel.png',
        featured: false,
        description:'In love with sports and healthy food, I like to share my workouts with people and have fun.'
        },
        {
        id: 4,
        name:'Ann (France)',
        image: '/assets/images/ann.png',
        featured: false,
        description:'I am a fashion lover. I often buy clothes, makeup, shoes and bags. I like to take care of my appearance doing sports and with natural beauty products.'
        },
        {
        id: 5,
        name:'Dylan (England)',
        image: '/assets/images/dylan.png',
        featured: false,
        description:'I like reading, devotion lawyer, I would like to improve every day in my studies and become a great lawyer.'
        }
    ];