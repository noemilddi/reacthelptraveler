import * as ActionTypes from './ActionTypes';
import { USERS } from '../shared/users'

export const initialState = {
    users: USERS
}

export const Users = (state = initialState, action) => {
    switch(action.type) {
        case ActionTypes.ADD_USERS:
            return {...state, users: action.payload}

        default:
            return state;
    }
}