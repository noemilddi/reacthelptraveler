import * as ActionTypes from './ActionTypes';

export const addUsers = (users) => ({
    type: ActionTypes.ADD_USERS,
    payload: users
});

export const addComments = (comments) => ({
    type: ActionTypes.ADD_COMMENTS,
    payload: comments
});

export const addLeaders = (leaders) => ({
    type: ActionTypes.ADD_LEADERS,
    payload: leaders
});

export const postComment = (userId, rating, author, comment) => (dispatch) => {

    const newComment = {
        userId: userId,
        rating: rating,
        author: author,
        comment: comment,
	    date: new Date().toISOString()
    };

    console.log(newComment);

    setTimeout(() => {
	    dispatch(addComment(newComment))
    }, 500);
}

export const addComment = (comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: comment
});

