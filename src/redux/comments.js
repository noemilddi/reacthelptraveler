import * as ActionTypes from './ActionTypes';
import { COMMENTS } from '../shared/comments';

export const initialState = {
    comments: COMMENTS
}

export const Comments = (state = initialState, action) => {
    switch(action.type) {
        case ActionTypes.ADD_COMMENTS:
            console.log(state)
            return {...state, comments: action.payload}

        case ActionTypes.ADD_COMMENT:
            const comment = { ...action.payload, id: state.comments.length};
            return {...state, comments: state.comments.concat(comment)};
      
        
        default:
            return state;
           
    }
}