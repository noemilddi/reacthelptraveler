import * as ActionTypes from './ActionTypes';
import { LEADERS } from '../shared/leaders'

export const initialState = {
    leaders: LEADERS
}

export const Leaders = (state = initialState, action) => {
    switch(action.type) {
        case ActionTypes.ADD_LEADERS:
            return {...state, leaders: action.payload}

        default:
            return state;
    }
}