import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Users } from './users';
import { Comments } from './comments';
import { Leaders } from './leaders';
import thunk from 'redux-thunk';

export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            users: Users,
            comments: Comments,
            leaders: Leaders,
        }),
        applyMiddleware(thunk)
    ); 
    return store;
}