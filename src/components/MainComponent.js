import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import About from './AboutComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import Contact from './ContactComponent';
import Users from './UsersComponent';
import UserDetail from './UsersdetailComponent';
import { addUsers, addComments, addLeaders, postComment } from '../redux/ActionCreators';

const mapStateToProps = state => {
  return {
    users: state.users,
    comments: state.comments,
    leaders: state.leaders
  }
}

const mapDispatchToProps = dispatch => ({
  addUsers: (users) => {dispatch(addUsers(users))},
  addComments: (comments) => {dispatch(addComments(comments))},
  addLeaders: (leaders) => {dispatch(addLeaders(leaders))},
  postComment: (userId, rating, author, comment) => {dispatch(postComment(userId, rating, author, comment))}
});

class Main extends Component {

  render() {

    const HomePage = () => {
        return(
            <Home
            />
        );
      }

      const UserWithId = ({match}) => {
        return(
          <UserDetail user={this.props.users.users.filter((user) => user.id === parseInt(match.params.userId,10))[0]}
          comments={this.props.comments.comments.filter((comment) => comment.userId === parseInt(match.params.userId,10))}
          postComment={this.props.postComment}
          />
        );
      }

    return (
        <div>
            <Header />
                <Switch>
                    <Route path='/home' component={HomePage} />
                    <Route exact path="/users" component={() => <Users users={this.props.users} /> } />
                    <Route path="/users/:userId" component={UserWithId} />
                    <Route exact path="/aboutus" component={() => <About leaders={this.props.leaders} /> } />
                    <Route exact path="/contactus" component={Contact} />
                    <Redirect to="/home" />
                </Switch>
            <Footer />   
        </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));