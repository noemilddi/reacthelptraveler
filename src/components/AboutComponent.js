import React from 'react';
import { Breadcrumb, BreadcrumbItem, Media } from 'reactstrap';
import { Fade, Stagger } from 'react-animation-components';
import { Link } from 'react-router-dom';

function RenderLeader({ leader }){
    return(
	    <Fade in>
            <Media className="mt-5">
                <Media left>
                    <Media object src={leader.image} alt={leader.name} style={{width:'180px', height:'180px'}} />
                </Media>
                <Media body className="ml-5">
                    <Media heading>{leader.name}</Media>
                    <Media heading><h6>{leader.designation}</h6></Media>
                    <p className="d-none d-sm-block">{leader.description}</p>
                </Media>
            </Media>
	    </Fade>
    );
}

function About(props) {

        const leaders = props.leaders.leaders.map((leader) => {
            return(
                <div key={leader.id} className="col-12">
                    <RenderLeader leader={leader} />
                </div>
            );
        })

        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>About Us</BreadcrumbItem>
                    </Breadcrumb>
                </div>
                <div className="row row-content">
                    <div className="col-6">
                        <h2>Our History</h2>
                        <p>Our idea was formed from a group of students who liked to travel to know the world, other cultures, different people and, above all, to know as much as possible about the world we live in, that's why we think. Why not share our hobby with the rest of the world?</p>
                        <p>Help Traveler will help you organize your own trip in a traditional way, or contact users from all over the world who are willing to offer you a place in their life and show you their culture, customs and then you can do the same with them.</p>
                    </div>
                    <div>
                        <div className="col-6 offset-6">
                            <img src="assets/images/trip.jpg" alt="..." />
                        </div>
                    </div>
                </div>
                <Media list>
                    <Stagger in>
                        {leaders}
                    </Stagger>
                </Media>
            </div>

        );

  }

export default About;
