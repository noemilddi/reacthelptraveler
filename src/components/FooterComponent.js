import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return(
            <div className="footer">
            <div className="container">
                <div className="row">             
                   <div className="col-1 offset-md-4">
                    <a className="btn btn-social btn-social-icon btn-google" href="http://google.com/"><i className=" fa fa-google-plus fa-lg"></i></a>
                </div>
                   <div className="col-1">
                    <a className="btn btn-social btn-social-icon btn-facebook" href="http://www.facebook.com/"><i className=" fa fa-facebook fa-lg"></i></a>
                </div>
                   <div className="col-1">
                    <a className="btn btn-social btn-social-icon btn-twitter" href="http://twitter.com/"><i className=" fa fa-twitter fa-lg"></i></a>
                </div>
                    <div className="col-1">
                        <a className="btn btn-social btn-social-icon btn-linkedin" href="http://www.linkedin.com/"><i className=" fa fa-linkedin fa-lg"></i></a>
                    </div>
               </div>
            </div>
        </div>
        );
    }

}

export default Footer;