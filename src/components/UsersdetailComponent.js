import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Media, Button, Col, Row, Modal, ModalHeader, ModalBody, Label } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Fade, Stagger } from 'react-animation-components';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);

class CommentForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isNavOpen: false,
            isModalOpen: false
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleSubmit(values) {
        this.toggleModal();
        this.props.postComment(this.props.userId, values.select, values.name, values.message);
    }

    render() {
        return(
            <>
                <LocalForm>
                    <Row className="form-group">
                        <Col>
                            <Button outline onClick={this.toggleModal} >
                            <span className="fa fa-pencil fa-lg"></span> Sumbit Comment
                            </Button>
                        </Col>
                    </Row>
                </LocalForm>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className="form-group">
                                <Label for="rating" md={12}>Rating</Label>
                                <Col md={12}>
                                <Control.select model=".select" id="select" name="select"
                                    className="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Control.select>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="name" md={12}>Your Name</Label>
                                <Col md={12}>
                                    <Control.text model=".name" id="name" name="name"
                                        placeholder="Your Name"
                                        className="form-control"
                                        validators={{
                                            required, 
                                            minLength: minLength(3),
                                            maxLength: maxLength(15)
                                        }}
                                    />
                                    <Errors 
                                        className="text-danger"
                                        model=".name"
                                        show="touched"
                                        messages={{
                                            required: 'Required',
                                            minLength: 'Must be greater than 2 characters',
                                            maxLength: 'must be 15 characters or less'
                                        }}
                                    />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="message" md={12}>Comment</Label>
                                <Col md={12}>
                                    <Control.textarea model=".message" id="message" name="message"
                                        rows="6"
                                        className="form-control" />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={12}>
                                    <Button type="submit" color="primary">
                                        Submit
                                    </Button>
                                </Col>
                            </Row>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </>
        );
    }

}

function RenderUser({user}) {
    if (user != null){
        return (
            <div className="col-12 m-1">
                <Fade in>
            <Media className="mt-5">
                <Media left>
                    <Media object src={user.image} alt={user.name} style={{ width: '150px', height: '150px'}}/>
                </Media>
                <Media body className="ml-5">
                    <Media heading>{user.name}</Media>
                    <p className="d-none d-sm-block">{user.description}</p>
                </Media>
            </Media>
	    </Fade>
            </div>
        );
    }
    else{
        return(
            <div></div>
        )
    }
}

function RenderComments({comments, postComment, userId}) {
    if (comments != null){
        return(          
            <div className="col-12 m-1">
                <ul className="list-unstyled">
                    <Stagger in>
                    <h4>Comments</h4>
                        {comments.map((comment) => {
                            return (
                                <Fade in>
                                    <li key={comment.id}>
                                        <p>{comment.comment}</p>
                                        <p> --{comment.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
                                    </li>
                                </Fade>
                            );
                        })}
                    </Stagger>
                </ul>
                <CommentForm userId={userId} postComment={postComment} />
            </div>          
        );
    } 
    else {
        return(
            <div></div>
        );
    }
}

const UserDetail = (props) => {
    return(
        <div className="container">
             <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to='/users'>Users</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{props.user.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{props.user.name}</h3>
                    <hr />
            </div>
        </div>
            <div className="row">
                <RenderUser user={props.user} />
                <hr />
            </div>
            <div className="row">
                <div className="col offset-3">
                    <RenderComments comments={props.comments}
                        postComment={props.postComment}
                        userId={props.user.id} />
                </div>
            </div>
        </div>
    );
}

export default UserDetail