import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, Card, CardImg, CardBody, CardTitle, CardText,Form, FormGroup, Input, Label, Button, Breadcrumb, BreadcrumbItem } from 'reactstrap';

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            options: [
                {
                    id: 0,
                    name:'Flight',
                    image: '/assets/images/flight.jpg',
                    description:'Choose your flight to go to your favorite destination and enjoy our offers.'   
                },
                {
                    id: 1,
                    name: 'Hotel',
                    image: '/assets/images/hotel.jpg',
                    description: 'Choose your hotel to stay in your favorite destination and enjoy our offers.'
                },
                {
                    id: 2,
                    name: 'Flight + Hotel',
                    image: '/assets/images/pack.jpg',
                    description: 'Choose our flight+hotel and enjoy our offers to go to your favorite destination.'
                },
                {
                    id: 3,
                    name: 'Interchange',
                    image: '/assets/images/interchange.jpg',
                    description: 'Choose one of our registered users, share new experiences and get to know a new way of traveling.'
                }
            ],
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleLogin(event) {
        this.toggleModal();
        alert("You have a reservation:\n Username: " + this.username.value + " Departure Date: " + this.departuredate.value + " Return Date: " + this.returndate.value)
        event.preventDefault();
    }


    render() {
        const list = this.state.options.map((opt) => {
            return(
                <div key={opt.id} className="col-6">
                    <Card style={{marginTop:'50px', marginBottom:'50px'}}>
                        <CardImg width="100%" src={opt.image}/>
                        <CardBody>
                            <CardTitle>{opt.name}</CardTitle>
                            <CardText>{opt.description}</CardText>
                            <Button outline onClick={this.toggleModal}>
                                Booking now!
                            </Button>
                        </CardBody>
                    </Card>
                </div>
            );
        })
        return(
            <>
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem active>Home</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>Travel Options</h3>
                            <hr />
                            <h5>Choose between our different ways of traveling and enjoy the best prices!!</h5>
                        </div>
                        {list}
                    </div>
                </div>

                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
                <ModalBody>
                    <Form onSubmit={this.handleLogin}>
                    <FormGroup>
                            <Label htmlFor="username">Username</Label>
                            <Input type="text" id="username" name="username"
                            innerRef={(input) => this.username = input} />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="departuredate">Departure Date</Label>
                            <Input type="date" id="departuredate" name="departuredate"
                            innerRef={(input) => this.departuredate = input} />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="returndate">Return Date</Label>
                            <Input type="date" id="returndate" name="returndate"
                            innerRef={(input) => this.returndate = input} />
                        </FormGroup>
                        <Button type="submit" value="submit" color="primary">Reserve Now!</Button>
                    </Form>
                </ModalBody>
                </Modal>
            </>
        );

    }
        
}

export default Home;