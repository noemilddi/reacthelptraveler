import React from 'react';
import { Card, CardImg, CardImgOverlay, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';

function RenderUserList({ user }) {
    return(
        <Card style={{marginTop:"40px"}}>
            <Link to={`/users/${user.id}`}>
            <CardImg width="100%" object src={user.image} alt={user.name}/>
                <CardImgOverlay>
                    <CardTitle>{user.name}</CardTitle>
                </CardImgOverlay>
            </Link>
        </Card>
    );
}


const Users = (props) => {
    const list = props.users.users.map((user) => {
        return(
            <div key={user.id} className="col-12 col-md-5 m-1">
                <RenderUserList user={user} />
            </div>
        );
    });

    return(
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Users</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>Users</h3>
                        <hr />
                    <h5>Evaluate your interchange with our users and help your colleagues improve their experience!!</h5>
                </div>
            </div>
            <div className="row">
                {list}
            </div>
        </div>
    );
}
 
    
export default Users;